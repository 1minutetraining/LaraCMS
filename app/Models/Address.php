<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class);   //CREATE RELATION WITH USER MODEL
    }

    public function getFullState()    //NAME OF THIS METHOD JUST NAME WITHOUT LARAVEL RULE LIKE MUTANT OR ACCESSORY
    {
        //RETURN VALEU OF STATE FIELD ADN VALUE OF CITY FIELD TOGETHER
        return $this->state . ' / ' . $this->city;      //OR return $this->attributes['state'] .' / ' .$this->attributes['city'];
    }
}
