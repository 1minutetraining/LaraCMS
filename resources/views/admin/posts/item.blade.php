{{--WHEN USE BELOW LINE JUST USE THIS COMMAND(@INCLUDE('ADMIN.POSTS.ITEM')) IN LIST.BLADE INSTED OF THIS LINE WE USE THIS COMMAND(@EACH('admin.posts.item',$posts,'post')--}}
{{--@foreach($posts as $post)--}}
<tr>
    <th scope="row">{{ $post->post_id }}</th>
    <td>{{ $post->post_title }}</td>
    <td>{{ $post->post_author }}</td>
    <td>{{ $post->post_view_count }}</td>
    <td>{{ $post->created_at }}</td>
    <td>
        <span class="badge badge-{{ $post->post_status == 1 ? 'primary' : 'danger' }}">
            {{ $post->post_status == 1 ? 'فعال' : 'غیرفعال'}}
        </span>
    </td>
    <td>
        <a href="{{ route('admin.posts.delete',[$post->post_id]) }}">
            <i class="fa fa-trash-o"></i>
        </a>
        <a href="{{ route('admin.posts.edit',[$post->post_id]) }}">
            <i class="fa fa-edit"></i>
        </a>
    </td>
</tr>
{{--@endforeach--}}
