@extends('layouts.admin')
@section('content')
    <div class="card-title">
        <h4>لیست مطالب </h4>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            @include('partials.success')
            <table class="table">
                <thead>
                @include('admin.posts.columns')
                </thead>
                <tbody>
                @each('admin.posts.item',$posts,'post')  {{--like foreach($posts as post--}}
                </tbody>
            </table>
        </div>
@endsection