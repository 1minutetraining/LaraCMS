@extends('layouts.admin')
@section('content')
    <div class="card-title">
        <h4>لیست کاربران </h4>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            @include('partials.success')
            <table class="table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>نام و نام خانوادگی</th>
                    <th>ایمیل</th>
                    <th>موجودی</th>
                    <th>تاریخ ثبت نام</th>
                    <th>وضعیت</th>
                    <th>عملیات</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr>
                        <th scope="row">{{ $user->id }}</th>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->wallet }}</td>
                        <td>{{ $user->created_at }}</td>
                        <td>
                            <span class="badge badge-{{ $user->status==1 ? 'primary' : 'danger' }}">{{ $user->status == 1 ? 'فعال' : 'غیرفعال'}}</span>
                        </td>
                        <td>
                            <a href="{{ route('admin.users.delete',[$user->id]) }}"> {{--SECOND ARGOMAN IS INPUT AND PUT TO USER_ID THAT EXPLAIN IN ROUTE--}}
                                <i class="fa fa-trash-o"></i> {{--TRASH ICON--}}
                            </a>
                            <a href="{{ route('admin.users.edit',[$user->id]) }}">
                                <i class="fa fa-edit"></i> {{--EDIT ICON--}}
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
@endsection