<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'userFullName' => 'required',
            'userEmail' => 'required|email',
            'userpassword' => 'required|max:4'
        ];
    }

    public function messages()
    {
        return [
            'userFullName.required' => 'وارد کردن نام کاربری الزامی می باشد',
            'userEmail.required' => 'وارد کردن ایمیل الزامی می باشد',
            'userEmail.email' => 'فرمت ایمیل وارد شده اشتباه می باشد',
            'userpassword.max' => 'رمز عبور حتما کمتر از 4 حرف باشد',
            'userpassword.required' => 'وارد کردن پسورد الزامی می باشد'
        ];
    }
}
