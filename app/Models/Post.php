<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    const PUBLISHED = 1;
    const FUTURE = 2;
    const DRAFT = 3;
    const PENDING = 4;
    protected $primaryKey = 'post_id';   //WHEN NAME OF ID'S FIELD WAS NOT ID, MUST DESCRIBE IT HERE

    // protected $fillable = []; //WITHE LIST   LIST OF FIELDS CAN INSERT TO TABLE
    protected $guarded = [];     //BLACK LIST   LIST OF FIELDS CAN NOT INSERT TO TABLE

    // protected $guarded = []; OR protected $guarded = ['id'];    INSERT ALL OF FILEDS TO TABLE

    public function user()
    {
        $this->belongsTo(User::class,'post_author');
    }

    /*TAKE VALUE FROM CRAETE METHOD'S POSTCONTROLLER*/
    public function setPostSlugAttribute($value)   //IT IS A MUTANT AND IT SET DATA WHEN IT SAVE DATA
    {
        //$this->attributes['post_slug'] = str_slug($value);   //BEST FOR ENGLISH CONTENTS
        $this->attributes['post_slug'] = preg_replace('/\s+/', '-', $value);
    }

    public static function postStatuses(int $status = null)
    {
        $statuses = [
            self::FUTURE => 'زمان بندی',
            self::DRAFT => 'پیش نویس',
            self::PENDING => 'بازبینی',
            self::PUBLISHED => 'منتشر شده',
        ];
        if (!is_null($status) && in_array($status, array_keys($statuses))) {
            return $statuses[$status];
        }
        return $statuses;
    }
}
