<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $text='welcome';
        return view('welcome',compact('text'));

    }
}
