<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'userFullName' => 'required',
            'userEmail'=>'required|email|unique:users,id,'.$this->id,
        ];
    }

    public function messages()
    {
        return [
            'userFullName.required' => 'وارد کردن نام کاربری الزامی می باشد',
            'userEmail.required' => 'وارد کردن ایمیل الزامی می باشد',
            'userEmail.unique' => 'ایمیل وارد شده قبلا ثبت شده است',
            'userEmail.email' => 'ایمیل را صحیح وارد نمایید',

        ];

    }
}
