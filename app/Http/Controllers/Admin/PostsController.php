<?php

namespace App\Http\Controllers\admin;

use App\Http\Requests\PostCreateRequest;
use App\Models\Post;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PostsController extends Controller
{
    public function index()
    {
        $posts = Post::get();
        return view('admin.posts.list', compact('posts'));
    }

    public function create()
    {
        $postStatuses = Post::postStatuses();
        return view('admin.posts.create', compact('postStatuses'));
    }


    public function store(PostCreateRequest $request)
    {
        $post = Post::create([
            'post_title' => $request->input('postTiltle'),
            'post_content' => $request->input('postContent'),
            'post_author' => User::first()->id ,  //OR Auth::id()
            'post_slug' =>$request->input('postTiltle') ,  //OR str_slug($request->input('postTitle'))  THIS LINE USE setPostSlugAttribute METHOD FROM POST MODEL
            'post_status'=>$request->input('postStatus')
        ]);
        if ($post && $post instanceof Post) {
            return back()->with('status', 'ثبت مطلب با موفقیت انجام شد');
        }
    }


    public function delete($post_id)
    {
        $post = Post::find($post_id);
        $deletResualt = $post->delete();
        if ($deletResualt) {
            return back()->with('status', 'کاربر با موفقیت حذف گردید');
        } else {
            return back()->with('status', 'خطا در حذف کاربر');
        }

    }

    public function edit(Request $request, $post_id)
    {
        //$postRoles = Post::getPostRole();
        $post = Post::find($post_id);
        return view('admin.posts.edit', compact('post', 'postRoles'));
    }

    public function update(PostUpdateRequest $request, $post_id)
    {
        $post = Post::find($post_id);
        if ($post && $post instanceof Post) {
            $postData = [
                'name' => $request->input('postFullName'),
                'email' => $request->input('postEmail'),
                'role' => $request->input('postRole'),
            ];
            if ($request->filled('postPassword')) {
                $postData['password'] = $request->input('postPassword');
            }
            $updateResult = $post->update($postData);
            if ($updateResult) {
                return redirect()->route('admin.posts')->with('status', 'کاربر با موفقیت به روز رسانی گردید!');
            }
        }
    }
}

