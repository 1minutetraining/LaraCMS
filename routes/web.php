<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*Route::get('/',function (){
    return view('welcome');
});*/

//SIMPLE WAY OF ROUTING
/*Route::get('/','Homecontroller@index');
Route::get('admin','Admin\Admincontroller@index');
Route::get('admin/users','Admin\Userscontroller@index')->name('admin.users');
Route::get('admin/users/create','Admin\Userscontroller@create')->name('admin.users.create');
Route::post('admin/users/store','Admin\Userscontroller@store')->name('admin.users.store');
Route::get('admin/users/delete/{user_id}','Admin\Userscontroller@delete')->name('admin.users.delete');
//{USER-ID} IS A VARIABLE THAT WHICH USER WANT TO DELETE AND IIT IS A INPUT IN DELETE METHOD IN USERCONTROLLER
//THIS VARIABLE WILL FILL IN LIST.BLADE
Route::get('admin/users/edit/{user_id}','Admin\Userscontroller@edit')->name('admin.users.edit');
Route::post('admin/users/update/{user_id}','Admin\Userscontroller@update')->name('admin.users.update');*/

//GROUPING OF ROUTES
/*Route::get('/','Homecontroller@index');
Route::prefix('admin',function (){
    Route::get('/','Admin\Admincontroller@index')->name('admin');
    Route::get('users','Admin\Userscontroller@index')->name('admin.users');
    Route::get('users/create','Admin\Userscontroller@create')->name('admin.users.create');
    Route::post('users/store','Admin\Userscontroller@store')->name('admin.users.store');
    Route::get('users/delete/{user_id}','Admin\Userscontroller@delete')->name('admin.users.delete');
    Route::get('users/edit/{user_id}','Admin\Userscontroller@edit')->name('admin.users.edit');
    Route::post('users/update/{user_id}','Admin\Userscontroller@update')->name('admin.users.update');
})*/

//GROUPING OF ROUTE WITH NAMESPACE
Route::get('/', 'Homecontroller@index');
Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {
    Route::get('/','Admincontroller@index')->name('admin');
    Route::group(['prefix' => 'users'], function () {
    Route::get('/','Userscontroller@index')->name('admin.users');
    Route::get('create','Userscontroller@create')->name('admin.users.create');
    Route::post('store','Userscontroller@store')->name('admin.users.store');
    Route::get('delete/{user_id}','Userscontroller@delete')->name('admin.users.delete');
    Route::get('edit/{user_id}','Userscontroller@edit')->name('admin.users.edit');
    Route::post('update/{user_id}','Userscontroller@update')->name('admin.users.update');
    });
    Route::group(['prefix' => 'posts'], function () {
        Route::get('/','Postscontroller@index')->name('admin.posts');
        Route::get('create','Postscontroller@create')->name('admin.posts.create');
        Route::post('store','Postscontroller@store')->name('admin.posts.store');
        Route::get('delete/{post_id}','Postscontroller@delete')->name('admin.posts.delete');
        Route::get('edit/{post_id}','Postscontroller@edit')->name('admin.posts.edit');
        Route::post('update/{post_id}','Postscontroller@update')->name('admin.posts.update');
        });
});


