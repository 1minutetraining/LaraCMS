<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Models\Address;
use App\User;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{
    public function index()
    {
        $users = User::get();  //OR $users = User::ALL();  //OR $users =User::where('wallet','>',12000)->get();
        //dd($users);
        return view('admin.users.list', compact('users'));
    }

    public function create()
    {
        $userRoles = User::getUserRole();

        /* $sample=[
            'a','b'
        ]; */
        //return view('layouts.admin.users.create',['key's name*that sen to blade file*'=>'$sample']);

        return view('admin.users.create', compact('userRoles'));
    }

    /**
     * @param Request $request
     */
//  public function store(Request $request)
    public function store(UserCreateRequest $request)
    {
        // FIRST WAY FOR FILL TABLE
        /*$user=new User();
        $user->name='maryam karami';
        $user->email='karami@yahoo.com';
        $user->password=bcrypt(123);
        $user->wallet=150000;
        $user->save();*/

        //SECOND WAY FOR FILL TABLE
        /*User::create([
            'name'=>'maryam karami',
            'email'=>'karami@yahoo.com;
        ]);*/

        //THIRD WAY FOR FILL TABLE
        /*THESE CODES MOVE TO UserCreateRequest
            $request->validate([
            'userFullName' => 'required',
            'userEmail' => 'required|email',
            'userpassword' => 'required|max:4'
        ],
            [
                'userFullName.required' => 'وارد کردن نام کاربری الزامی می باشد',
                'userEmail.required' => 'وارد کردن ایمیل الزامی می باشد',
                'userEmail.email' => 'فرمت ایمیل وارد شده اشتباه می باشد',
                'userpassword.max' => 'رمز عبور حتما کمتر از 4 حرف باشد',
                'userpassword.required' => 'وارد کردن پسورد الزامی می باشد'
            ]);*/

        $user = User::create([
            'name' => $request->input('userFullName'),
            'email' => $request->input('userEmail'),
            'password' => $request->input('userpassword'),
            'role' => $request->input('userRole')
        ]);
        /* dd($request->all());
         dd($user);*/
        if ($user && $user instanceof User) {
            //return back();
            //return redirect()->route('admin.users');

            return back()->with('status', 'ثبت نام با موفقیت انجام شد');
        }
    }

    //public function delete(Request $request,$user_id)  //REQUEST IS AVAIBLE
    public function delete($user_id)                    //$USER-ID EXPLAIN IN ROUTE
    {
        //dd($user_id);

        //FIRAT WAY FOR DELETE
        /* $user=User::find($user_id); //ARGOMAN CAN BE ARRAY OF IDS THAT RETURN A COLLECTION OF OBJECTS OF USER
           $user->delete();*/

        //SECOND WAY FOR DELETE
        //User::destroy($user_id);  //ARGOMAN CAN BE ARRAY OF IDS THAT DELETE SEVRAL USERS DELETE TOGETHER

        $user = User::find($user_id);
        $deletResualt = $user->delete();
        if ($deletResualt) {
            return back()->with('status', 'کاربر با موفقیت حذف گردید');
        } else {
            return back()->with('status', 'خطا در حذف کاربر');
        }

    }

    public function edit(Request $request, $user_id)
    {
        //dd($user_id,$request);
        //$user=User::findorfaild($user_id);  //IF NOT FIND USER_ID CREATE ERROR WHEN WE DONT FOURCE TO CREATE ERROR

        $userRoles = User::getUserRole();
        $user = User::find($user_id);
        //dd($user->address);                         //RETURN OBJECT FROM ADDRESS CLASS
        //dd($user->address->state);                 //FOR USE RELATION
        //dd($user->address->getFullState());       //GETFULLSTATE METHOD EXPLAIN IN ADDRESS MODEL
        //dd($user->address[0]->getFullState());   //WHEN RELATION IS 1->N USE []
        //$address=Address::find(1);             //FIND ADDRESS'S ID=1 AND RETURN OBJECT FROM USER CLSS
        //dd($address->user);

        return view('admin.users.edit', compact('user', 'userRoles'));
    }

    public function update(UserUpdateRequest $request, $user_id)
    {
        /* $user=User::find($user_id);
         if ($user && $user instanceof User){
             $user->update([
                 'name'=>$request->input('userFullName'),
                 'email'=>$request->input('userEmail'),  //USUALLY DONT CHHANGE IT
                 'password'=>$request->input('userEmail')   //IF SEND NULL HAPPEN A PROBLEM IN DB
             ]);
         }*/

        $user = User::find($user_id);
        if ($user && $user instanceof User) {
            $userData = [
                'name'  => $request->input('userFullName'),
                'email' => $request->input('userEmail'),
                'role' => $request->input('userRole'),
            ];
            if ($request->filled('userPassword')) { //IF THE FIELD EXSIST AND FILL
                $userData['password'] = $request->input('userPassword');
            }
            $updateResult = $user->update($userData);
            if ($updateResult) {
                return redirect()->route('admin.users')->with('status', 'کاربر با موفقیت به روز رسانی گردید!');
            }
        }
    }
}


