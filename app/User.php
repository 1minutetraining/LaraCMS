<?php

namespace App;

use App\Models\Address;
use App\Models\Post;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /* protected $table='new name';
     protected $primaryKey='new name';
     const CREATED_AT = 'new name';
     const UPDATED_AT = 'new name';
     protected $timestams=false;
     protected $dates=[
         'expire_at'
     ];*/

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    const USER = 1;
    const ADMIN = 2;
    protected $fillable = [
        'name', 'email', 'password', 'role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    //IN NAME OF THIS METHOD GET AND ATTRIBUTE ARE CONST MIDDLE NAME IS NAME OF FILED
    public function getWalletAttribute($value)    //IT IS ACCESSORY AND IT CHANGE DATA WHEN TAKE DATA
    {
        return number_format($value);    //DONT USE THIS METHOD NO WHERE
    }

    public function address()
    {
        //FOR 1->1 RELATION
        //return $this->hasOne(Address::class);   //return $this->hasOne('\\App\Models\Address');   //CREATE RELATION WITH ADDRESS MODEL .RETURN OBJECT OF ADDRESS MODEL
    }

    public function posts()
    {
        //FOR 1->N RELATION
        //WHEN FOREINKEY IS DIFFRENT FROM LARAVEL DEFAULT MUST BE GIVE IT IN SECOND ARGOMAN
        return $this->hasMany(Post::class, 'post_author'); //RETURN COLLECTION OF POST MODEL
    }

    public static function getUserRole()
    {
        return [
            self::USER => 'کاربر عادی',
            self::ADMIN => 'کاربر مدیر'
        ];
    }
}

