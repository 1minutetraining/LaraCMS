@extends('layouts.admin')
@section('content')
    <div class="card-title">
        <h4>ایجاد مطلب جدید </h4>
    </div>

    <div class="card-body">
        <div class="row">
            <div class="col-md-12">
                <div class="basic-form p-10">
                    @include('partials.errors')
                    @include('partials.success')
                    <form method="post" action="{{ route('admin.posts.store') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="postTiltle">عنوان</label>
                            <input id="postTiltle" name="postTiltle" type="text"
                                   class="form-control input-default hasPersianPlaceHolder"
                                   value="{{ old('postTiltle') }}">
                        </div>
                        <div class="form-group">
                            <label for="postContent">محتوا</label>
                            <textarea id="postContent" name="postContent" rows="10"
                                      class="form-control input-default"
                                      value=""
                            >{{ old('postContent') }}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="postStatus">وضعیت</label>
                            <select id="postStatus" name="postStatus" class="form-control persianText">
                                @foreach($postStatuses as $poststatus => $poststatustitle)
                                    <option value="{{ $poststatus }}">{{ $poststatustitle }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group m-t-20">
                            <button type="submit" class="btn btn-primary m-b-10 m-l-5">ثبت اطلاعات
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection